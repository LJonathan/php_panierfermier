
<?php
require 'mod_header/vue/headerVue.php';

$tpl = new Smarty();


//Data necessaires a l'affichage des infos profil
$nom = $_SESSION['nom'];
$prenom = $_SESSION['prenom'];
$mail = $_SESSION['mail'];
$typeInformation = 'disabled';
$action = "modifierInformations";
$texteAction = "Modifier";

If (isset($_REQUEST['action']) AND ( $_REQUEST['action'] == 'modifierInformations')) {
    $typeInformation = '';
    $action = "validerInformations";
    $texteAction = "Valider";
}


// Data necessaires a l'affichage des infos mot de passe
$formMotDePasse = NULL;
$action2 = 'modifierMotDePasse';
$texteAction2 = 'Modifier Mot de Passe';

If (isset($_REQUEST['action']) AND ( $_REQUEST['action'] == 'modifierMotDePasse')) {
    $formMotDePasse = 'Mot de passe actuel :<br>'
                       . '<input type="password" name ="password" value=""><br>'
                       . 'Nouveau Mot de passe :<br>'
                       . '<input type="password" name ="newPassword" value=""><br>'
                       . 'Verification nouveau mot de passe :<br>'
                       . '<input type="password" name ="newPasswordTest" value=""><br>';
    $action2 = 'validerMotDePasse';
    $texteAction2 = 'Valider';
}

if (!isset($msg)){
    $msg = "";
}

$tpl->assign('typeInformation', $typeInformation);
$tpl->assign('nom', $nom);
$tpl->assign('prenom', $prenom);
$tpl->assign('mail', $mail);
$tpl->assign('action', $action);
$tpl->assign('texteAction', $texteAction);
$tpl->assign('formMotDePasse', $formMotDePasse);
$tpl->assign('action2', $action2);
$tpl->assign('texteAction2', $texteAction2);
$tpl->assign('msgPassword', $msg);


$tpl->display('mod_profil/vue/profilVue.tpl');

