<?php

// Chargement des fonctions de contrôle de validité des champs
require_once('include/utilitaires.php');

// Affichage par défaut
function vueParDefaut() {
    verificationConnexion("profil");
    require_once 'mod_profil/vue/profilVue.php';
}


// Initialisation de la modification d'informations personnelles
function modifierInformations($parameters = null) {
    verificationConnexion("profil");
    require_once 'mod_profil/vue/profilVue.php';
}

// Validation de la modification d'informations personnelles
function validerInformations() {
    verificationConnexion("profil");

    // verification nom
    $tab_nom = modificationValidite("nom", $_POST['nom']);

    // verification prenom
    $tab_prenom = modificationValidite("prenom", $_POST['prenom']);

    // verification mail
    $tab_mail = modificationValidite("mail", $_POST['mail']);

    // Si tout les modifications sont valides
    if ($tab_nom["validite"] AND $tab_prenom["validite"] AND $tab_mail["validite"] AND ( $tab_nom["modification"] || $tab_prenom["modification"] || $tab_mail["modification"])) {
        require_once 'mod_profil/modele/profilModele.php';

        // Si l'adresse mail a été changée
        if ($tab_mail["modification"]) {
            // Si l'adresse mail n'est pas utilisée : 
            // On effectue les changement dans la table INDIVIDU
            $resultat = individuParMail($tab_mail["tmp"]);
            if ($resultat->rowCount() == 0) {
                majIndividu($tab_nom["tmp"], $tab_prenom["tmp"], $tab_mail["tmp"], $_SESSION['ID']);

                echo 'Les modifications ont été validées (mail compris). <br>';
                $_SESSION['nom'] = $tab_nom["tmp"];
                $_SESSION['prenom'] = $tab_prenom["tmp"];
                $_SESSION['mail'] = $tab_mail["tmp"];

                // Si l'adresse mail est déjà utilisée
            } else {
                echo 'Cette adresse mail est déjà enregistrée. <br>';
            }
            // Si l'adresse mail n'a pas été changée
        } else {
            majIndividu($tab_nom["tmp"], $tab_prenom["tmp"], $tab_mail["tmp"], $_SESSION['ID']);
            
            echo 'Les modifications ont été validées (mail non changé). <br>';
            $_SESSION['nom'] = $tab_nom["tmp"];
            $_SESSION['prenom'] = $tab_prenom["tmp"];
            $_SESSION['mail'] = $tab_mail["tmp"];
        }
        // 1+ champs modifiés invalides :
    } else {
        echo 'Erreur: Champ(s) invalide(s)';
    }

    require_once 'mod_profil/vue/profilVue.php';
}


// Initialisation de la modification de mot de passe
function modifierMotDePasse() {
    verificationConnexion("profil");
    require_once 'mod_profil/vue/profilVue.php';
}

function validerMotDePasse() {
    verificationConnexion("profil");
    $msg = "";
    
    $password = $_POST['password'];
    $newPassword = $_POST['newPassword'];
    $newPasswordTest = $_POST['newPasswordTest'];
    
    $val_password = validiteChamp('password', $password);
    $val_newPassword = validiteChamp('password', $newPassword);
    $val_newPasswordTest = ($newPassword == $newPasswordTest);
    
    If($val_password AND $val_newPassword AND $val_newPasswordTest){
        require_once 'mod_profil/modele/profilModele.php';
        
        $data = passwordById($_SESSION['ID']);
        $row = $data->fetch(PDO::FETCH_ASSOC);

        If (cryptage($password) == $row['MPAIND']){
            updatePasswordById(cryptage($newPassword), $_SESSION['ID']);
            $msg = "Mot de passe changé avec succès <br>";
            require_once 'mod_profil/vue/profilVue.php';
        } else {
            $msg = $msg . "Mot de passe incorrect <br>";
            require_once 'mod_profil/vue/profilVue.php';            
        }
    } else {
        if(!$val_password){
            $msg = $msg . "Mot de passe invalide <br>";
        }
        if(!$val_newPassword){
            $msg = $msg . "Nouveau mot de passe invalide <br>";
        }
        if(!$val_newPasswordTest){
            $msg = $msg . "Les mots de passe ne sont pas identiques <br>";
        }
        require_once 'mod_profil/vue/profilVue.php'; 
    }
}



// Fonction de contrôle de modification de champ + validite   
// Si modification = false alors validite = true
// Return tab[$tmp, bool modification, bool validite]
function modificationValidite($type, $value) {
    $modification = false;
    $validite = true;
    $tmp = strtolower($value);
    if ($_SESSION[$type] != $tmp) {
        $modification = true;
        $validite = (validiteChamp($type, $tmp));
    }
    $tab = ["tmp" => $tmp, "modification" => $modification, "validite" => $validite];
    return $tab;
}