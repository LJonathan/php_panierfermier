<?php

function validiteChamp($type, $tmp) {
    switch ($type) {
        case 'nom';
        case 'prenom':
            return (validiteNomPrenom($tmp));
        case 'mail':
            return (validiteMail($tmp));
        case 'password' :
            return (validitePassword($tmp));
    }
}

function validiteNomPrenom($texte) {
    if (strlen($texte) < 2 || strlen($texte) > 31) {
        return false;
    } else {
        return true;
    }
}

function validiteMail($texte) {
    if (strlen($texte) < 3 || strlen($texte) > 31) {
        return false;
    } else {
        return true;
    }
}

function validitePassword($texte) {
    if (strlen($texte) < 3 || strlen($texte) > 31) {
        return false;
    } else {
        return true;
    }
}

function verificationConnexion($redirection = NULL) {
    if (!isset($_SESSION['ID'])) {
        if (!empty($redirection)) {
            header('Location: index.php?gestion=login&action=connexion&redirection=' . $redirection);
            exit;
        } else {
            header('Location: index.php?gestion=login&action=connexion');
            exit;
        }
    }
}

function cryptage($password){
    $gauche = "br25&x%";
    $droite = "rj!@";
    $newPassword = hash('ripemd128', "$gauche$password$droite");
    return $newPassword;
}

// A MODIFIER / SUPPRIMER / DEPLACER
// Fonction Echo *ERREUR*
function afficheErreur($msg) {
    echo 'Erreur : loginControleur.php<br>'
    . $msg . '<br>';
}
