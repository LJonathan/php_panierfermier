
<?php
$tpl = new Smarty();

// GESTION CONNECTION
$nom = '';
$lien1 = '<a href="index.php?gestion=login&action=connexion">Connexion</a>';
$lien2 = '<a href="index.php?gestion=login&action=inscription">Inscription</a>';

// SI CONNECTE REMPLACER CONNEXION INSCRIPTION PAR DECONNEXION
if(isset($_SESSION['ID'])){
    $nom = 'Compte : ' . $_SESSION['prenom'] . ' ' . $_SESSION['nom'];
    $lien1 = '<a href="index.php?gestion=login&action=deconnexion">Deconnexion</a>';
    $lien2 = '';
}

// ASSIGNATION VARIABLES SMARTY
$tpl->assign('username',$nom);
$tpl->assign('lien1', $lien1);
$tpl->assign('lien2', $lien2);
$tpl->assign('accueil', '<a href="index.php?gestion=accueil">Accueil</a>');
$tpl->assign('produits', '<a href="index.php?gestion=produits">Produits</a>');
$tpl->assign('producteurs', '<a href="index.php?gestion=producteurs">Producteurs</a>');
$tpl->assign('association', '<a href="index.php?gestion=association">Association</a>');
$tpl->assign('contact', '<a href="index.php?gestion=contact">Contact</a>');
$tpl->assign('distribution', '<a href="index.php?gestion=distribution">Distribution</a>');
$tpl->assign('profil', '<a href="index.php?gestion=profil">Profil</a>');
$tpl->assign('admin', '<a href="index.php?gestion=admin">admin</a>');

// AFFICHAGE
$tpl->display('mod_header/vue/headerVue.tpl');
