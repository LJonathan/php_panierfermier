<?php

require_once('mod_' . $gestion . '/controleur/' . $gestion . 'Controleur.php');

if (isset($_REQUEST['action'])) {

    switch ($_REQUEST['action']) {


        case 'connexion':
            //L'utilisateur demande la connexion
            connexion($_REQUEST);
            break;
       
        case 'inscription' :
            // Rechercher l'enregistrement
            inscription($_REQUEST);
            break;
        
        case 'deconnexion' :
            deconnexion($_REQUEST);
            break;
        
        case 'controleInscription' :
            controleInscription($_REQUEST);
            break;
        
        case 'controleConnexion' :
            controleConnexion($_REQUEST);
            break;
        
        case 'modifierInformations' :
            modifierInformations($_REQUEST);
            break;
        
        case 'validerInformations' :
            validerInformations($_REQUEST);
            break;
        
        case 'modifierMotDePasse' :
            modifierMotDePasse($_REQUEST);
            break;
        
        case 'validerMotDePasse' :
            validerMotDePasse($_REQUEST);
            break;
        
        default:
            echo 'Erreur de destination';
            break;
    }
    
} else {

    vueParDefaut($_REQUEST);
}