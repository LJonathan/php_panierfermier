-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  mer. 29 avr. 2020 à 17:27
-- Version du serveur :  5.7.28
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `panierfermier`
--

-- --------------------------------------------------------

--
-- Structure de la table `annee`
--

DROP TABLE IF EXISTS `annee`;
CREATE TABLE IF NOT EXISTS `annee` (
  `IANNEE` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Année',
  `PCOANN` decimal(5,2) NOT NULL COMMENT 'Valeur cotisation',
  PRIMARY KEY (`IANNEE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article_generique`
--

DROP TABLE IF EXISTS `article_generique`;
CREATE TABLE IF NOT EXISTS `article_generique` (
  `IARGEN` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Article Generique',
  `LARGEN` varchar(50) NOT NULL COMMENT 'Libelle Article Generique',
  `IPHPRO` int(10) DEFAULT NULL COMMENT 'ID Photo Produit',
  `TYARTI` varchar(15) NOT NULL COMMENT 'Type Article',
  PRIMARY KEY (`IARGEN`),
  KEY `fk_iphpro` (`IPHPRO`),
  KEY `fk_tyarti` (`TYARTI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `article_producteur`
--

DROP TABLE IF EXISTS `article_producteur`;
CREATE TABLE IF NOT EXISTS `article_producteur` (
  `IARPRO` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant',
  `IPRODU` int(10) NOT NULL COMMENT 'ID Producteur',
  `IARGEN` int(10) NOT NULL COMMENT 'ID Article generique',
  `CARPRO` int(10) NOT NULL COMMENT 'Contenance',
  `UNITE` varchar(10) NOT NULL COMMENT 'Unite',
  `PUNARP` decimal(6,2) NOT NULL COMMENT 'Prix',
  `AAGBIO` enum('true','false') DEFAULT NULL COMMENT 'Agriculture biologique',
  `IPHPRO` int(10) DEFAULT NULL COMMENT 'ID photo',
  `DARPRO` tinyint(1) DEFAULT NULL COMMENT 'Disponibilite',
  PRIMARY KEY (`IARPRO`),
  KEY `fk_iprodu` (`IPRODU`),
  KEY `fk_iargen` (`IARGEN`),
  KEY `fk_unite` (`UNITE`),
  KEY `fk_iphpro2` (`IPHPRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `ICOMMA` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Commande',
  `IINDIV` int(10) NOT NULL COMMENT 'ID Individu',
  `IDISTR` int(10) NOT NULL COMMENT 'ID Distribution',
  `DVACOM` datetime NOT NULL COMMENT 'Date Heure validation commande',
  `DVAPCO` datetime DEFAULT NULL COMMENT 'Date Heure validation paiement commande',
  `PCOMMA` enum('true','false') NOT NULL COMMENT 'Commande payee',
  PRIMARY KEY (`ICOMMA`),
  KEY `fk_iindiv2` (`IINDIV`),
  KEY `fk_idistr` (`IDISTR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande_article`
--

DROP TABLE IF EXISTS `commande_article`;
CREATE TABLE IF NOT EXISTS `commande_article` (
  `ICOMMA` int(10) NOT NULL COMMENT 'ID Commande',
  `IARPRO` int(10) NOT NULL COMMENT 'ID Article_producteur',
  `QARCOM` int(10) NOT NULL COMMENT 'Nombre exemplaire article',
  `PARCOM` decimal(6,2) NOT NULL COMMENT 'Prix article unitaire',
  PRIMARY KEY (`ICOMMA`,`IARPRO`),
  KEY `fk_iarpro` (`IARPRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cotisation`
--

DROP TABLE IF EXISTS `cotisation`;
CREATE TABLE IF NOT EXISTS `cotisation` (
  `IANNEE` int(10) NOT NULL COMMENT 'ID Annee',
  `IINDIV` int(10) NOT NULL COMMENT 'ID Individu',
  `CPAYEE` enum('true','false') NOT NULL COMMENT 'cotisation payee',
  PRIMARY KEY (`IANNEE`,`IINDIV`),
  KEY `fk_iindiv3` (`IINDIV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cpvill`
--

DROP TABLE IF EXISTS `cpvill`;
CREATE TABLE IF NOT EXISTS `cpvill` (
  `CPVILL` int(10) NOT NULL COMMENT 'Code Postal',
  `NVILLE` varchar(45) NOT NULL COMMENT 'Nom Ville',
  PRIMARY KEY (`CPVILL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `distribution`
--

DROP TABLE IF EXISTS `distribution`;
CREATE TABLE IF NOT EXISTS `distribution` (
  `IDISTR` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Distribution',
  `ILIEU` int(10) NOT NULL COMMENT 'ID lieu',
  `DDISTR` datetime NOT NULL COMMENT 'Date Heure Debut distribution',
  `HFIDIS` time DEFAULT NULL COMMENT 'Heure fin Distribution',
  PRIMARY KEY (`IDISTR`),
  KEY `fk_ilieu2` (`ILIEU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `individu`
--

DROP TABLE IF EXISTS `individu`;
CREATE TABLE IF NOT EXISTS `individu` (
  `IINDIV` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Individu',
  `NINDIV` varchar(30) NOT NULL COMMENT 'Nom individu',
  `PINDIV` varchar(30) NOT NULL COMMENT 'Prenom individu',
  `MINDIV` varchar(40) NOT NULL COMMENT 'Mail individu',
  `MPAIND` varchar(255) NOT NULL COMMENT 'Mot de passe',
  `TINDIV` int(13) DEFAULT NULL COMMENT 'Telephone adherent',
  PRIMARY KEY (`IINDIV`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `jour`
--

DROP TABLE IF EXISTS `jour`;
CREATE TABLE IF NOT EXISTS `jour` (
  `NJOUR` varchar(10) NOT NULL COMMENT 'Jour',
  PRIMARY KEY (`NJOUR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `jour_horaire`
--

DROP TABLE IF EXISTS `jour_horaire`;
CREATE TABLE IF NOT EXISTS `jour_horaire` (
  `NJOUR` varchar(10) NOT NULL COMMENT 'Jour',
  `ISTAND` int(10) NOT NULL COMMENT 'ID Stand',
  `HOUMAT` time DEFAULT NULL COMMENT 'Heure ouverture matin',
  `HFEMAT` time DEFAULT NULL COMMENT 'Heure fermeture matin',
  `HOUAPR` time DEFAULT NULL COMMENT 'Heure ouverture apres midi',
  `HFEAPR` time DEFAULT NULL COMMENT 'Heure fermeture apres midi',
  PRIMARY KEY (`NJOUR`,`ISTAND`),
  KEY `fk_istand` (`ISTAND`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `lieu`
--

DROP TABLE IF EXISTS `lieu`;
CREATE TABLE IF NOT EXISTS `lieu` (
  `Ilieu` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID lieu',
  `NRULIE` int(5) DEFAULT NULL COMMENT 'Numéro de rue',
  `ALIEU` varchar(30) NOT NULL COMMENT 'Adresse',
  `ALIEU2` varchar(30) DEFAULT NULL COMMENT 'Complément Adresse',
  `CLIEU` varchar(40) DEFAULT NULL COMMENT 'Commentaire Adresse',
  `CPVILL` int(10) NOT NULL COMMENT 'CP ville',
  PRIMARY KEY (`Ilieu`),
  KEY `fk_icpvil` (`CPVILL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `paiement`
--

DROP TABLE IF EXISTS `paiement`;
CREATE TABLE IF NOT EXISTS `paiement` (
  `IPAIEM` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Paiement',
  `IINDIV` int(10) NOT NULL COMMENT 'ID individu',
  `TPAIEM` varchar(15) NOT NULL COMMENT 'Type paiement',
  `VPAIEM` decimal(7,2) NOT NULL COMMENT 'Valeur paiement',
  `DPAIEM` date NOT NULL COMMENT 'Date paiement',
  PRIMARY KEY (`IPAIEM`),
  KEY `fk_iindiv` (`IINDIV`),
  KEY `fk_tpaiem` (`TPAIEM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `photo_produit`
--

DROP TABLE IF EXISTS `photo_produit`;
CREATE TABLE IF NOT EXISTS `photo_produit` (
  `IPHPRO` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Photo',
  `NPHPRO` varchar(30) NOT NULL COMMENT 'Nom Photo Produit',
  `DPHPRO` varchar(50) DEFAULT NULL COMMENT 'Description Photo Produit',
  PRIMARY KEY (`IPHPRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `producteur`
--

DROP TABLE IF EXISTS `producteur`;
CREATE TABLE IF NOT EXISTS `producteur` (
  `IPRODU` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Producteur',
  `NPRODU` varchar(30) NOT NULL COMMENT 'Nom Producteur',
  `PPRODU` varchar(20) DEFAULT NULL COMMENT 'Prenom Producteur',
  `ILIEU` int(10) NOT NULL COMMENT 'ID Adresse Producteur',
  `NENPRO` varchar(30) NOT NULL COMMENT 'Nom Entreprise Producteur',
  `TPRODU` int(13) DEFAULT NULL COMMENT 'Telephone Producteur',
  `TPUPRO` int(13) DEFAULT NULL COMMENT 'Telephone Public Producteur',
  `EPRODU` varchar(40) NOT NULL COMMENT 'Email Producteur',
  `PRPROD` varchar(500) DEFAULT NULL COMMENT 'Presentation publique du producteur',
  `PHPROD` varchar(40) DEFAULT NULL COMMENT 'Nom Photo Producteur',
  PRIMARY KEY (`IPRODU`),
  KEY `fk_ilieu` (`ILIEU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stand`
--

DROP TABLE IF EXISTS `stand`;
CREATE TABLE IF NOT EXISTS `stand` (
  `ISTAND` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID Stand',
  `ILIEU` int(10) NOT NULL COMMENT 'ID Lieu',
  `IPRODU` int(10) NOT NULL COMMENT 'ID Producteur',
  PRIMARY KEY (`ISTAND`),
  KEY `fk_ilieu3` (`ILIEU`),
  KEY `fk_iprodu3` (`IPRODU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_article`
--

DROP TABLE IF EXISTS `type_article`;
CREATE TABLE IF NOT EXISTS `type_article` (
  `TYARTI` varchar(15) NOT NULL COMMENT 'Type Article',
  PRIMARY KEY (`TYARTI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_article_producteur`
--

DROP TABLE IF EXISTS `type_article_producteur`;
CREATE TABLE IF NOT EXISTS `type_article_producteur` (
  `IPRODU` int(10) NOT NULL COMMENT 'ID producteur',
  `TYARTI` varchar(15) NOT NULL COMMENT 'Type article',
  PRIMARY KEY (`IPRODU`,`TYARTI`),
  KEY `fk_tyarti2` (`TYARTI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_paiement`
--

DROP TABLE IF EXISTS `type_paiement`;
CREATE TABLE IF NOT EXISTS `type_paiement` (
  `TPAIEM` varchar(15) NOT NULL COMMENT 'Type Paiement',
  PRIMARY KEY (`TPAIEM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `unite`
--

DROP TABLE IF EXISTS `unite`;
CREATE TABLE IF NOT EXISTS `unite` (
  `UNITE` varchar(10) NOT NULL COMMENT 'Unite de mesure',
  PRIMARY KEY (`UNITE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article_generique`
--
ALTER TABLE `article_generique`
  ADD CONSTRAINT `fk_iphpro` FOREIGN KEY (`IPHPRO`) REFERENCES `photo_produit` (`IPHPRO`),
  ADD CONSTRAINT `fk_tyarti` FOREIGN KEY (`TYARTI`) REFERENCES `type_article` (`TYARTI`);

--
-- Contraintes pour la table `article_producteur`
--
ALTER TABLE `article_producteur`
  ADD CONSTRAINT `fk_iargen` FOREIGN KEY (`IARGEN`) REFERENCES `article_generique` (`IARGEN`),
  ADD CONSTRAINT `fk_iphpro2` FOREIGN KEY (`IPHPRO`) REFERENCES `photo_produit` (`IPHPRO`),
  ADD CONSTRAINT `fk_iprodu` FOREIGN KEY (`IPRODU`) REFERENCES `producteur` (`IPRODU`),
  ADD CONSTRAINT `fk_unite` FOREIGN KEY (`UNITE`) REFERENCES `unite` (`UNITE`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_idistr` FOREIGN KEY (`IDISTR`) REFERENCES `distribution` (`IDISTR`),
  ADD CONSTRAINT `fk_iindiv2` FOREIGN KEY (`IINDIV`) REFERENCES `individu` (`IINDIV`);

--
-- Contraintes pour la table `commande_article`
--
ALTER TABLE `commande_article`
  ADD CONSTRAINT `fk_iarpro` FOREIGN KEY (`IARPRO`) REFERENCES `article_producteur` (`IARPRO`),
  ADD CONSTRAINT `fk_icomma` FOREIGN KEY (`ICOMMA`) REFERENCES `commande` (`ICOMMA`);

--
-- Contraintes pour la table `cotisation`
--
ALTER TABLE `cotisation`
  ADD CONSTRAINT `fk_iannee` FOREIGN KEY (`IANNEE`) REFERENCES `annee` (`IANNEE`),
  ADD CONSTRAINT `fk_iindiv3` FOREIGN KEY (`IINDIV`) REFERENCES `individu` (`IINDIV`);

--
-- Contraintes pour la table `distribution`
--
ALTER TABLE `distribution`
  ADD CONSTRAINT `fk_ilieu2` FOREIGN KEY (`ILIEU`) REFERENCES `lieu` (`Ilieu`);

--
-- Contraintes pour la table `jour_horaire`
--
ALTER TABLE `jour_horaire`
  ADD CONSTRAINT `fk_istand` FOREIGN KEY (`ISTAND`) REFERENCES `stand` (`ISTAND`),
  ADD CONSTRAINT `fk_njour` FOREIGN KEY (`NJOUR`) REFERENCES `jour` (`NJOUR`);

--
-- Contraintes pour la table `lieu`
--
ALTER TABLE `lieu`
  ADD CONSTRAINT `fk_icpvil` FOREIGN KEY (`CPVILL`) REFERENCES `cpvill` (`CPVILL`);

--
-- Contraintes pour la table `paiement`
--
ALTER TABLE `paiement`
  ADD CONSTRAINT `fk_iindiv` FOREIGN KEY (`IINDIV`) REFERENCES `individu` (`IINDIV`),
  ADD CONSTRAINT `fk_tpaiem` FOREIGN KEY (`TPAIEM`) REFERENCES `type_paiement` (`TPAIEM`);

--
-- Contraintes pour la table `producteur`
--
ALTER TABLE `producteur`
  ADD CONSTRAINT `fk_ilieu` FOREIGN KEY (`ILIEU`) REFERENCES `lieu` (`Ilieu`);

--
-- Contraintes pour la table `stand`
--
ALTER TABLE `stand`
  ADD CONSTRAINT `fk_ilieu3` FOREIGN KEY (`ILIEU`) REFERENCES `lieu` (`Ilieu`),
  ADD CONSTRAINT `fk_iprodu3` FOREIGN KEY (`IPRODU`) REFERENCES `producteur` (`IPRODU`);

--
-- Contraintes pour la table `type_article_producteur`
--
ALTER TABLE `type_article_producteur`
  ADD CONSTRAINT `fk_iprodu2` FOREIGN KEY (`IPRODU`) REFERENCES `producteur` (`IPRODU`),
  ADD CONSTRAINT `fk_tyarti2` FOREIGN KEY (`TYARTI`) REFERENCES `type_article` (`TYARTI`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
